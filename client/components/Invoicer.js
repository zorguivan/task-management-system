import React from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { Button, Checkbox, Container, Card, Input, Select, Segment, Grid, Label, TextArea, Form, Icon, Table, Modal, Divider } from 'semantic-ui-react'
import { single, save, list, listbyrange, registerCRUDReducers, remove } from '../services/ActionsService';
import { getFormattedDate, getPreviousMonth } from '../Utils/Utilities';

registerCRUDReducers('clients', 'client');
registerCRUDReducers('invoices', 'invoice');

class Invoicer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { startRange: getFormattedDate(getPreviousMonth()), endRange: getFormattedDate() }
    }

    componentWillMount() {
        let getClients = list.bind(list, 'clients');
        getClients();
        console.log(this.props);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.clients != this.state.clients) {
            this.updateLocalState(newProps.clients);
        }
    }

    updateLocalState(clientsList) {
        console.log(clientsList)
    }

    handleOpen(modalName) {
        let state = this.state;
        state[modalName] = true;
        this.setState(state);
    }

    handleClose(modalName) {
        let state = this.state;
        state[modalName] = false;
        this.setState(state);
    }
    
    handleChange(target, value) {
        let state = this.state;
        state[target] = value;
        this.setState(state);
    }
    rearrange(date){
        let holder = date.split('/');
        return [holder[1], holder[0], holder[2]].join('/')
    }
    search() {
        console.log('Searching')
        this.setState({ searching: true });
        let a = new Date(this.rearrange(this.state.startRange));
        let b = new Date(this.rearrange(this.state.endRange));

        let startStamp = a.getTime();
        let endStamp = b.getTime() + 86340000;
        let total = 0;
        let { logs } = this.props;
        if (logs.size > 0) {
            logs.forEach((log) => {
                if (log.get('startTime') >= startStamp && log.get('endTime') <= endStamp) {
                    total += log.get('totalTime')
                }
            });
        }
        this.setState({ searching: false, selectedTime: total });
        console.log(total)

        return total;
    }

    addClient() {
        let saveClient = save.bind(save, 'client', 'id');
        let holder = Immutable.Map({
            'clientName': this.state.clientName,
            'clientEmail': this.state.clientEmail,
            'clientCompany': this.state.clientCompany,
            'clientLocation': this.state.clientLocation,
            'clientPostalCode': this.state.clientPostalCode,
            'clientAccount': this.state.clientAccount,
            'clientSortCode': this.state.clientSortCode
        });
        
        saveClient(holder);
        let getClients = list.bind(list, 'clients');
        getClients();
        this.handleClose('additionModal')
    }

    handleChange(target, v) {
        let state = this.state;
        state[target] = v.target.value;
        this.setState(state);
    }

    removeClient(id) {
        let deleteClient = remove.bind(remove, 'client');
        deleteClient(id).then((p) => {
            let getClients = list.bind(list, 'clients');
            getClients();
        }).catch((err) => {
            console.log(err)
            console.log('Error Deleting Task');
        });
    }

    selectClient(name){
        this.props.clients.forEach((client) => {
            if(client.get('clientName') == name){
                this.setState({selectedClient: client.get('_id')})
            }
        })
    }

    generateInvoice(){
        let createInvoice = save.bind(save, 'invoice', 'id');
        let holder = Immutable.Map({
            'client': this.state.selectedClient,
            'hours': Math.round((this.state.selectedTime/ 1000 / 60) * 100) / 100
        });
        createInvoice(holder);
    }

    handleSelect(e){
        let x = e.target.innerText;
        let client = this.selectClient(x);
    }

    render() {
        let clients = this.props.clients || [];
        let taskId = this.props.taskid;
        let options = [];
        if(clients.size > 0){
            clients.forEach((client) => {
                options.push({key: client.get('_id'), value: client.get('_id'), text: client.get('clientName')});
            });
        }
        return (
            <Container>
                <Card fluid centered={true} style={{ marginTop: '40px', padding: "20px" }}>
                    <Card.Content>
                        <Label><h3>Invoice</h3></Label>
                        <Modal size='mini' open={this.state.additionModal} trigger={<Button color="teal" floated="right" icon labelPosition='left' onClick={this.handleOpen.bind(this, 'additionModal')}><Icon name='plus' />Add Client</Button>}>
                            <Modal.Header>New Client Creation</Modal.Header>
                            <Modal.Content>
                                <Label>Client Name</Label>
                                <Input fluid placeholder='Name Goes Here' onChange={this.handleChange.bind(this, 'clientName')} />
                                <Divider />
                                <Label>Client Email</Label>
                                <Input fluid placeholder='Example@email.com' onChange={this.handleChange.bind(this, 'clientEmail')} />
                                <Divider />
                                <Label>Client Company Name</Label>
                                <Input fluid placeholder='Company Name Goes Here' onChange={this.handleChange.bind(this, 'clientCompany')} />
                                <Divider />
                                <Label>Company Location</Label>
                                <Input fluid placeholder='Location Goes Here' onChange={this.handleChange.bind(this, 'clientLocation')} />
                                <Divider />
                                <Label>Company Postal Code</Label>
                                <Input fluid placeholder='Name Goes Here' onChange={this.handleChange.bind(this, 'clientPostalCode')} />
                                <Divider />
                                <Label>Bank Account</Label>
                                <Input fluid placeholder='Name Goes Here' onChange={this.handleChange.bind(this, 'clientAccount')} />
                                <Divider />
                                <Label>Sort Code</Label>
                                <Input fluid placeholder='Name Goes Here' onChange={this.handleChange.bind(this, 'clientSortCode')} />
                            </Modal.Content>
                            <Modal.Actions>
                                <Button color='grey' onClick={this.handleClose.bind(this, 'additionModal')}>
                                    <Icon name='cancel' /> Cancel
                            </Button>
                                <Button color='green' onClick={this.addClient.bind(this)}>
                                    <Icon name='check' /> Save
                            </Button>
                            </Modal.Actions>
                        </Modal>
                        <Modal size='small' open={this.state.listingModal} trigger={<Button color="violet" floated="right" icon labelPosition='left' onClick={this.handleOpen.bind(this, 'listingModal')}><Icon name='list' />View Clients</Button>}>
                            <Modal.Header>Clients List</Modal.Header>
                            <Modal.Content>
                                <Table celled inverted selectable>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell collapsing>Client Name</Table.HeaderCell>
                                            <Table.HeaderCell collapsing>Client Email</Table.HeaderCell>
                                            <Table.HeaderCell collapsing>Delete</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {clients.map((client, index) => {
                                            return <Table.Row key={index} >
                                                <Table.Cell>{client.get('clientName')}</Table.Cell>
                                                <Table.Cell>{client.get('clientEmail')}</Table.Cell>
                                                <Table.Cell textAlign='right'><Button color="red" animated='fade'>
                                                    <Button.Content visible><Icon name="delete" /></Button.Content>
                                                    <Button.Content hidden onClick={this.removeClient.bind(this, client.get('_id'))}>Delete</Button.Content>
                                                </Button>
                                                </Table.Cell>
                                            </Table.Row>
                                        })}
                                    </Table.Body>
                                </Table>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button color='grey' onClick={this.handleClose.bind(this, 'listingModal')}>
                                    <Icon name='close' /> Close
                            </Button>
                            </Modal.Actions>
                        </Modal>
                    </Card.Content>
                    <Card.Content >
                        <Container >
                            <Label>Start Range</Label>
                            <Input placeholder='dd/mm/yyyy' size="large" onChange={this.handleChange.bind(this, 'startRange')} style={{ marginRight: '40px' }} value={this.state.startRange} />
                            <Label>End Range</Label>
                            <Input placeholder='dd/mm/yyyy' size="large" onChange={this.handleChange.bind(this, 'endRange')} style={{ marginRight: '40px' }} value={this.state.endRange} />
                            {this.state.searching && <Button loading primary>
                                Search
                        </Button> || <Button primary onClick={this.search.bind(this)}>Search</Button>}
                        </Container>
                        {this.state.selectedTime > 0 && <Container style={{ marginRight: '40px'}}>
                        <Label color="blue" style={{marginLeft: '40px'}}>
                            {Math.round((this.state.selectedTime/ 1000 / 60) * 100) / 100 + ' ~ Hours'}
                            </Label>
                            <Select placeholder='Select client' options={options}  onChange={this.handleSelect.bind(this)} style={{marginRight: '40px'}} />
                            <Button color='orange' onClick={this.generateInvoice.bind(this)}>
                                    Create Invoice
                            </Button>
                        </Container>}
                    </Card.Content>
                </Card>
            </Container>
        );
    }
}

export default connect((state) => {
    console.log('This is the state:: ', state)
    return { clients: state.get('clients') }
})(Invoicer);

