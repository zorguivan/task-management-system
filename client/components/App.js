import React, { Component } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { Icon, Label, Menu, Table, Button, Segment, Container, Divider } from 'semantic-ui-react';
import { list, registerCRUDReducers, remove } from '../services/ActionsService';
import { Link } from 'react-router-dom';

registerCRUDReducers('tasks', 'task');

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    let getTasks = list.bind(list, 'tasks');
    getTasks();
  }

  removeTask(task) {
    let deleteTask = remove.bind(remove, 'task');
    deleteTask(task).then((p) => {
      let getTasks = list.bind(list, 'tasks');
      getTasks();
    }).catch((err) => {
      console.log(err)
      console.log('Error Deleting Task');
    });
  }

  namePriority(pr) {
    let val;
    switch (parseInt(pr)) {
      case 1:
        val = "Low"
        break;
      case 2:
        val = "Normal"
        break;
      case 3:
        val = "High"
        break;
      default:
        val = ""
    }
    return val;
  }

  render() {
    let tasks = this.props.tasks || [];
    let priorityClasses = [{}, { background: "#BDE5F8", color: '#00529B' }, { background: "#FEEFB3", color: '#9F6000' }, { background: "#FFD2D2", color: '#D8000C' }];
    return (
      <div>
        <Container textAlign='right'>
          <Divider hidden={true}></Divider>
          <Button inverted color='green' as={Link} to={`/taskcreation`}>+ Add Task</Button>
        </Container>
        <Divider hidden={true}></Divider>
        <Container >
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>#</Table.HeaderCell>
                <Table.HeaderCell>Title</Table.HeaderCell>
                <Table.HeaderCell>Priority</Table.HeaderCell>
                <Table.HeaderCell>Deadline</Table.HeaderCell>
                <Table.HeaderCell>Delete</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {tasks.map((task, index) => {
                return <Table.Row key={index}  >
                  <Table.Cell><Button as={Link} to={`/tasks/${task.get('_id')}`} color="blue">{index}</Button></Table.Cell>
                  <Table.Cell>{task.get('title')}</Table.Cell>
                  <Table.Cell style={priorityClasses[task.get('priority')]}>{this.namePriority(task.get('priority'))}</Table.Cell>
                  <Table.Cell>{task.get('deadline')}</Table.Cell>
                  <Table.Cell><Button size="small" color="red" onClick={this.removeTask.bind(this, task.get('_id'))}><Icon fitted={true} name='delete' /></Button></Table.Cell>
                </Table.Row>
              })}
            </Table.Body>
          </Table>
        </Container>
      </div>
    );
  }
}

export default connect((state) => {
  console.log('This is the state:: ', state)
  return { tasks: state.get('tasks') }
})(App);