import React from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { Button, Checkbox, Container, Card, Input, Select, Segment, Grid, Label, TextArea, Form, Icon, Table, Modal } from 'semantic-ui-react'
import { single, save, listbyid, registerCRUDReducers, remove } from '../services/ActionsService';
import { Slider } from 'react-semantic-ui-range';
import { Link, Redirect } from 'react-router-dom';
import Invoicer from './Invoicer'
import {timeConverter} from '../Utils/Utilities';

registerCRUDReducers('logs', 'log');

class Task extends React.Component {
    constructor(props) {
        super(props);
        this.interval = null
        this.state = { modalOpen: false }
    }

    handleOpen() {
        this.setState({ modalOpen: true })
    }

    handleClose() {
        this.setState({ modalOpen: false })
    }

    componentDidMount() {
        let getTask = single.bind(single, 'tasks', 'task');
        let getLogs = listbyid.bind(listbyid, 'logs');
        getTask(this.props.taskid);
        getLogs(this.props.taskid);
        console.log(this.props)
    }

    componentWillReceiveProps(newProps) {
        if (newProps.selectedtask != this.state.selectedtask) {
            this.updateLocalState(newProps.selectedtask);
        }
        if (newProps.logs != this.state.logs) {
            this.manageLogs(newProps.logs);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    handleChange(target, value) {
        let state = this.state;
        state[target] = value;
        this.setState(state);
    }

    saveTask() {
        let state = this.state;
        let task = Immutable.Map({
            'title': state.title,
            'deadline': state.deadline,
            'priority': state.priority,
            'estimate': state.estimate,
            'description': state.description,
            '_id': this.props.selectedtask.get('_id')
        });
        let saveTask = save.bind(save, 'task', '_id');

        saveTask(task).then(() => {
            console.log('Task Saved');
        }).catch((err) => {
            console.log(err)
            console.log('Error Saving Task');
        });

        this.setState({ redirect: '/' });
    }
    clearState() {
        this.setState({})
    }
    updateLocalState(props) {
        let state = this.state;
        state.description = props.get('description');
        state.title = props.get('title');
        state.estimate = parseInt(props.get('estimate'));
        state.deadline = props.get('deadline');
        state.priority = props.get('priority');
        state.selectedtask = true;
        this.setState(state)
        console.log(this.state)
    }

    timer(timestamp) {
        var today = new Date();

        let target = today.getTime() - timestamp;
        let milliseconds = parseInt((target % 1000) / 100),
            seconds = parseInt((target / 1000) % 60),
            minutes = parseInt((target / (1000 * 60)) % 60),
            hours = parseInt((target / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        this.setState({ timer: hours + ":" + minutes + ":" + seconds })
    }

    manageLogs(logs) {
        logs.forEach((log) => {
            if (!log.has('endTime') && log.has('startTime')) {
                this.setState({ logging: true, startTime: log.get('startTime'), taskId: log.get('taskId'), logId: log.get('_id') });
                this.timer(log.get('startTime'));
                this.interval = setInterval(() => {
                    this.timer(log.get('startTime'));
                }, 1000);
            }
        })
    }

    initiateLog() {
        var today = new Date();
        let state = this.state;
        let log = Immutable.Map({
            'taskId': this.props.taskid,
            'startTime': today.getTime()
        });

        let saveLog = save.bind(save, 'log', 'id');
        saveLog(log).then(() => {
            console.log('Log Saved');
        }).catch((err) => {
            console.log(err)
            console.log('Error Saving Log');
        });
        this.setState({ logging: true, startTime: today.getTime() });
        this.timer(today.getTime())
        this.interval = setInterval(() => {
            this.timer(today.getTime());
        }, 1000);
    }

    endLog() {
        var today = new Date();
        let state = this.state;
        let log = Immutable.Map({
            'taskId': this.state.taskId,
            'startTime': this.state.startTime,
            'endTime': today.getTime(),
            'summary': this.state.summary,
            'totalTime': today.getTime() - parseInt(this.state.startTime),
            '_id': this.state.logId
        });
        let saveLog = save.bind(save, 'log', '_id');
        saveLog(log).then(() => {
            let getLogs = listbyid.bind(listbyid, 'logs');
            getLogs(this.props.taskid).then(() => {
                this.setState({ modalOpen: false, logging: false });

            });
        }).catch((err) => {
            console.log(err)
            console.log('Error Saving Log');
        });

    }

    render() {
        let logs = this.props.logs || [];

        let priorities = [{ key: 'low', value: 1, text: 'Low Priortity' },
        { key: 'normal', value: 2, text: 'Normal Priortity' },
        { key: 'high', value: 3, text: 'High Priortity' }
        ];
        let sliderSettings = {
            start: 0,
            min: 0,
            max: 30,
            step: 1,
            onChange: (value) => {
                this.setState({
                    estimate: value
                })
            }
        }
        if (this.state && this.state.redirect) {
            return <Redirect to={this.state.redirect} />;
        }
        return (
            <div>
                <Container>
                    <Card fluid centered={true} style={{ marginTop: '40px', padding: "20px" }}>
                        <Card.Content>
                        <Label><h3>Task : {this.state.title || ''}</h3></Label>
                            {this.state.logging &&
                                <Modal open={this.state.modalOpen} trigger={<Button color="red" floated="right" icon labelPosition='left' onClick={this.handleOpen.bind(this)}><Icon name='stop' />{this.state.timer}</Button>}>
                                    <Modal.Header>What did you do ?</Modal.Header>
                                    <Modal.Content>
                                        <Form>
                                            <TextArea rows={3} style={{ minHeight: 100 }} autoHeight={true} placeholder='What did you work on ?' onChange={(e, { value }) => this.handleChange('summary', value)} />
                                        </Form>
                                    </Modal.Content>
                                    <Modal.Actions>
                                        <Button color='grey' onClick={this.handleClose.bind(this)}>
                                            <Icon name='cancel' /> Cancel
                                        </Button>
                                        <Button color='green' onClick={this.endLog.bind(this)}>
                                            <Icon name='check' /> Save
                                        </Button>
                                    </Modal.Actions>
                                </Modal>
                                || <Button color="green" floated="right" icon labelPosition='left' onClick={this.initiateLog.bind(this)}><Icon name='play' />Start Timer</Button>}
                        </Card.Content>
                        <Card.Content>
                            <Container>
                                <Input label="Title" placeholder='Title here' value={this.state.title || ''} onChange={(e, { value }) => this.handleChange('title', value)} />
                                <Input label="Deadline" placeholder='DD/MM/YYYY' value={this.state.deadline || ''} style={{ marginLeft: '40px' }} onChange={(e, { value }) => this.handleChange('deadline', value)} />
                                <Select placeholder='Select Priority' value={parseInt(this.state.priority) || ''} options={priorities} style={{ marginLeft: '40px' }} onChange={(e, { value }) => this.handleChange('priority', value)} />
                            </Container>
                            <Container style={{ marginTop: '30px' }}>
                                <Label>
                                    Estimate
                                </Label>
                                <Slider color="blue" inverted={false}
                                    value={this.state.estimate} settings={sliderSettings} />
                                <Label color="blue">Estimate of : {this.state.estimate}</Label>
                            </Container>
                            <Container style={{ marginTop: '30px' }}>
                                <Label>Description</Label>
                                <Form>
                                    <TextArea rows={3} style={{ minHeight: 100 }} autoHeight={true} placeholder='Description' value={this.state.description || ''} onChange={(e, { value }) => this.handleChange('description', value)} />
                                </Form>
                            </Container>
                            <Container style={{ marginTop: '30px' }} textAlign='right'>
                                <Button.Group>
                                    <Button onClick={this.clearState.bind(this)} as={Link} to={`/`}>Cancel</Button>
                                    <Button.Or />
                                    <Button color="blue" onClick={this.saveTask.bind(this)}>Save</Button>
                                </Button.Group>
                            </Container>
                        </Card.Content>
                    </Card>
                    <Container style={{ marginTop: '30px' }}>
                        <Card fluid centered={true} style={{ marginTop: '40px', padding: "20px" }}>
                            <Label><h3>Logs</h3></Label>
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell collapsing>Start Time</Table.HeaderCell>
                                        <Table.HeaderCell collapsing>End Time</Table.HeaderCell>
                                        <Table.HeaderCell collapsing>Total Time</Table.HeaderCell>
                                        <Table.HeaderCell collapsing>Summary</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {logs.map((log, index) => {
                                        if (log.has('endTime')) {
                                            return <Table.Row key={index} >
                                                <Table.Cell>{timeConverter(log.get('startTime'))}</Table.Cell>
                                                <Table.Cell>{timeConverter(log.get('endTime'))}</Table.Cell>
                                                <Table.Cell>{Math.round((log.get('totalTime') / 1000 / 60) * 100) / 100 + ' mins'}</Table.Cell>
                                                <Table.Cell>{log.get('summary')}</Table.Cell>
                                            </Table.Row>
                                        }
                                    })}
                                </Table.Body>
                            </Table>
                        </Card>
                    </Container>
                    </Container >
                <Invoicer taskid={this.props.taskid} logs={this.props.logs}/>
            </div>
        );
    }
}


export default connect((state) => {
    console.log('This is the state:: ', state)
    return { selectedtask: state.get('selectedtask'), logs: state.get('logs') }
})(Task);