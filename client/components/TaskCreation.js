import React from 'react';
import { Button, Checkbox, Container, Card, Input, Select, Segment, Grid, Label, TextArea, Form } from 'semantic-ui-react'
import { save, registerCRUDReducers } from '../services/ActionsService';
import { Slider } from 'react-semantic-ui-range';
import { Link } from 'react-router-dom';

class TaskCreation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value1: 5
        }
    }
    handleChange(target, value){
        let state = this.state;
        state[target] = value;
        this.setState(state);
    }
    saveTask(){
        let task = {};
        let state = this.state;
        let saveTask = save.bind(save, 'task', 'id');

        task.title = state.title;
        task.deadline = state.deadline;
        task.priority = state.priority;
        task.estimate = state.value1;
        task.description = state.description;

        saveTask(task).then(() => {
            console.log('Task Saved');
            this.props.history.push(`/`);
        }).catch((err) => {
            console.log(err)
            console.log('Error Saving Task');
        });
    }
    render() {
        let priorities = [{ key: 'low', value: 'low', text: 'Low Priortity' },
        { key: 'normal', value: 'normal', text: 'Normal Priortity' },
        { key: 'high', value: 'high', text: 'High Priortity' }
        ];
        let sliderSettings = {
            start: 5,
            min: 0,
            max: 30,
            step: 1,
        }

        return (
            <Container>
                <Card fluid centered={true} style={{ marginTop: '40px', padding: "20px" }}>
                    <Card.Content>
                        <Card.Header>Task Creation Menu</Card.Header>
                    </Card.Content>
                    <Card.Content>
                        <Container>
                            <Input label="Title" placeholder='Title here' onChange={(e, { value }) => this.handleChange('title', value)}/>
                            <Input label="Deadline" placeholder='DD/MM/YYYY' style={{ marginLeft: '40px' }} onChange={(e, { value }) => this.handleChange('deadline', value)}/>
                            <Select placeholder='Select Priority' options={priorities} style={{ marginLeft: '40px' }} onChange={(e, { value }) => this.handleChange('priority', value)}/>
                        </Container>
                        <Container style={{ marginTop: '30px' }}>
                            <Label>
                                Estimate
                            </Label>
                                <Slider color="blue" inverted={false}
                                    settings={{
                                        start: this.state.value1,
                                        min: 0,
                                        max: 30,
                                        step: 1,
                                        onChange: (value) => {
                                            this.setState({
                                                value1: value
                                            })
                                        }
                                    }} />
                            <Label color="blue">Estimate of : {this.state.value1}</Label>
                        </Container>
                        <Container style={{ marginTop: '30px' }}>
                            <Label>Description</Label>
                            <Form>
                                <TextArea rows={3} style={{ minHeight: 100 }} autoHeight={true} placeholder='Description' onChange={(e, { value }) => this.handleChange('description', value)} />
                            </Form>
                        </Container>
                        <Container style={{ marginTop: '30px' }} textAlign='right'>
                            <Button.Group>
                                <Button as={Link} to={`/`}>Cancel</Button>
                                <Button.Or />
                                <Button positive onClick={this.saveTask.bind(this)}>Save</Button>
                            </Button.Group>
                        </Container>
                    </Card.Content>
                </Card>
            </Container >
        );
    }
}



export default TaskCreation;