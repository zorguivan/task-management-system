import ReducersService from './ReducersService';
import Immutable from 'immutable';
import axios from 'axios';
import Store from '../Store';

export function registerCRUDReducers(entitymultiple, entitysingle) {
    ReducersService.registerReducer(`${entitymultiple.toUpperCase()}_READY`, (state, action) => {
        if (Immutable.Iterable.isIterable(action.data)) {
            return state.setIn([entitymultiple.toLowerCase()], action.data);
        }
        else {
            return state.setIn([entitymultiple.toLowerCase()], Immutable.fromJS(action.data));
        }
    });

    ReducersService.registerReducer(`SELECT_${entitysingle.toUpperCase()}`, (state, action) => {
        if (Immutable.Iterable.isIterable(action.data)) {
            return state.setIn([`selected${entitysingle.toLowerCase()}`], action.data);
        }
        else {
            return state.setIn([`selected${entitysingle.toLowerCase()}`], Immutable.fromJS(action.data));
        }
    });

    ReducersService.registerReducer(`${entitysingle.toUpperCase()}_READY`, (state, action) => {
        if (Immutable.Iterable.isIterable(action.data)) {
            return state.setIn([`selected${entitysingle.toLowerCase()}`], action.data);
        }
        else {
            return state.setIn([`selected${entitysingle.toLowerCase()}`], Immutable.fromJS(action.data))
        }
    });
}

export function select(singleEntityName, entity) {
    Store.dispatch({
        type: `SELECT_${singleEntityName.toUpperCase()}`,
        data: entity
    });
}

export function save(entityname, discriminator, entity) {
    return new Promise((resolve, reject) => {
        let toSave = Immutable.Iterable.isIterable(entity) ? entity.toJS() : entity;
        let promise = undefined;
        if (toSave[discriminator]) {
            let key = toSave[discriminator];
            promise = axios.put(`/api/${entityname}/${key}`, toSave);
        }
        else {
            promise = axios.post(`/api/${entityname}`, toSave);
        }
        promise.then((response) => {
            resolve(response);
        }).catch((err) => {
            Store.dispatch({
                type: 'ERROR',
                data: err
            });
            reject(err);
        });
    });
}

export function search(entityname, searchparams) {
    return new Promise((resolve, reject) => {
        axios.post(`/api/${entityname}/search`, searchparams).then((response) => {
            Store.dispatch({
                type: `${entityname.toUpperCase()}_READY`,
                data: response.data
            });
            resolve(response.data);
        }).catch((err) => {
            Store.dispatch({
                type: 'ERROR',
                data: err
            });
            reject(err);
        })
    });
}

export function list(entityname) {
    return new Promise((resolve, reject) => {
        console.log(`/api/${entityname}`)
        axios.get(`/api/${entityname}`).then((response) => {
            console.log(response)
            Store.dispatch({
                type: `${entityname.toUpperCase()}_READY`,
                data: response.data
            });
            resolve(response.data);
        }).catch((err) => {
            Store.dispatch({
                type: 'ERROR',
                data: err
            });
            reject(err);
        })
    })
}
export function listbyid(entityname, id) {
    return new Promise((resolve, reject) => {
        console.log(`/api/${entityname}`)
        axios.get(`/api/${entityname}/${id}`).then((response) => {
            console.log(response)
            Store.dispatch({
                type: `${entityname.toUpperCase()}_READY`,
                data: response.data
            });
            resolve(response.data);
        }).catch((err) => {
            Store.dispatch({
                type: 'ERROR',
                data: err
            });
            reject(err);
        })
    })
}

export function listbyrange(entityname, id, range) {
    return new Promise((resolve, reject) => {
        console.log(`/api/${entityname}`)
        axios.get(`/api/${entityname}/${id}/${range[0]}/${range[1]}`).then((response) => {
            console.log(response)
            Store.dispatch({
                type: `${entityname.toUpperCase()}_READY`,
                data: response.data
            });
            resolve(response.data);
        }).catch((err) => {
            Store.dispatch({
                type: 'ERROR',
                data: err
            });
            reject(err);
        })
    })
}

export function single(entityname, singularname, id) {
    return new Promise((resolve, reject) => {
        axios.get(`/api/${entityname}/${id}`).then((response) => {
            Store.dispatch({
                type: `${singularname.toUpperCase()}_READY`,
                data: response.data
            });
            resolve(response.data);
        });
    }).catch((err) => {
        dispatch({
            type: 'ERROR',
            data: err
        });
    });
}

export function remove(entityname, id) {
    if (id instanceof Object) {
        return new Promise((resolve, reject) => {
            axios.delete(`/api/${entityname}/${id.first}/${id.second}`).then(resolve).catch((err) => {
                Store.dispatch({
                    type: 'ERROR',
                    data: err
                });
                reject(err);
            });
        })
    }
    else {
        return new Promise((resolve, reject) => {
            axios.delete(`/api/${entityname}/${id}`).then(resolve).catch((err) => {
                Store.dispatch({
                    type: 'ERROR',
                    data: err
                });
                reject(err);
            });
        })
    }

}