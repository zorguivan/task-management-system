import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.js';
import Store from './Store';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import Main from './Main';
import createBrowserHistory from 'history/createBrowserHistory';

const history = createBrowserHistory();

window.store = Store;

ReactDOM.render(
  <Provider store={Store}>
    <Router history={history}>
      <Main history={history} />
    </Router>
  </Provider>,
  document.getElementById('root'));