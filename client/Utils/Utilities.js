export function timeConverter(timestamp) {
    let a = new Date(timestamp);
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let hour = a.getHours();
    let min = a.getMinutes();
    let sec = a.getSeconds();
    let time = date + ' ' + month + ' ' + year + '  At  ' + hour + ':' + min + ':' + sec;
    return time;
}

export function getFormattedDate(date) {
    var today = new Date()
    if(date) today = new Date(date);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return  dd + '/' + mm + '/' + yyyy;
}

export function getPreviousMonth(){
        let d = new Date();
        d.setMonth(d.getMonth() - 1);
        return d.getTime();
    }