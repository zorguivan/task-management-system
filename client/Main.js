import React from 'react';
import { Route, Switch } from 'react-router';
import App from './components/App';
import TaskCreation from './components/TaskCreation';
import Task from './components/Task';

class Main extends React.Component{
    render(){
        console.log('This is Main props');
        console.log(this.props);
        return (
            <Switch>
                <Route exact path="/" render={() => {
                    return <App />;
                }} />
                 <Route key="/taskcreation" path="/taskcreation" render={() => {
                    return <TaskCreation />;
                }} />
                <Route key=":task" exact path="/tasks/:taskid" render={(props) => {
                    return <Task taskid={props.match.params.taskid}/>
                }} />
            </Switch>
        );
    }
}

export default Main;