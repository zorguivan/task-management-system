import ReducersService from './services/ReducersService';
import {createStore} from 'redux';
import Immutable from 'immutable';

let store = createStore(ReducersService.reduce.bind(ReducersService), Immutable.fromJS(window.__PRELOADED_STATE__));

export default store;