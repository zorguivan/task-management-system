var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var logsRouts = require('./Routs/logs');
var clientsRouts = require('./Routs/clients');
var tasksRouts = require('./Routs/tasks');
var invoiceRouter = require('./Routs/invoice');
var port = process.env.PORT || 8080;

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/tms');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'dist')));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use('/api', clientsRouts);
app.use('/api', logsRouts);
app.use('/api', tasksRouts);
app.use('/api', invoiceRouter);
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.all('*', function (req, res) {
    res.redirect('/');
});

app.all('/*', function (req, res) {
    res.sendfile('client/index.html');
});

app.listen(port);
console.log('Server running on port : ' + port);
