module.exports = function template(invoice, client, totalHours) {
  var date = new Date().toDateString();
  var day = new Date();
  day.setDate(day.getDate() + 14); 
  var dueDate = day.toDateString()
  return `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <title>Example 1</title>
        <style>
          .clearfix:after {
      content: "";
      display: table;
      clear: both;
    }
    
    a {
      color: #5D6975;
      text-decoration: underline;
    }
    
    body {
      position: relative;
      width: 17cm;  
      height: 29.7cm; 
      margin: 0 auto; 
      color: #001028;
      background: #FFFFFF; 
      font-family: Arial, sans-serif; 
      font-size: 12px; 
      font-family: Arial;
    }
    
    header {
      padding: 10px 0;
      margin-bottom: 30px;
    }
    
    #logo {
      text-align: center;
      margin-bottom: 10px;
    }
    
    
    h1 {
      border-top: 1px solid  #5D6975;
      border-bottom: 1px solid  #5D6975;
      color: #5D6975;
      font-size: 2.4em;
      line-height: 1.4em;
      font-weight: normal;
      text-align: center;
      margin: 0 0 20px 0;
      background: url(dimension.png);
    }
    
    #project {
      float: left;
    }
    
    #project span {
      color: #5D6975;
      text-align: right;
      width: 52px;
      margin-right: 10px;
      display: inline-block;
      font-size: 0.8em;
    }
    
    #company {
      float: right;
      text-align: right;
    }
    
    #project div,
    #company div {
      white-space: nowrap;        
    }
    
    table {
      width: 100%;
      border-collapse: collapse;
      border-spacing: 0;
      margin-bottom: 20px;
    }
    
    table tr:nth-child(2n-1) td {
      background: #F5F5F5;
    }
    
    table th,
    table td {
      text-align: center;
    }
    
    table th {
      padding: 5px 20px;
      color: #5D6975;
      border-bottom: 1px solid #C1CED9;
      white-space: nowrap;        
      font-weight: normal;
    }
    
    table .service,
    table .desc {
      text-align: left;
    }
    
    table td {
      padding: 25px;
      text-align: right;
    }
    
    table td.service,
    table td.desc {
      vertical-align: top;
    }
    
    table td.unit,
    table td.qty,
    table td.total {
      font-size: 1.2em;
    }
    
    table td.grand {
      border-top: 1px solid #5D6975;;
    }
    
    #notices .notice {
      color: #5D6975;
      font-size: 1.2em;
    }
    
    footer {
      color: #5D6975;
      width: 100%;
      height: 30px;
      position: absolute;
      bottom: 0;
      border-top: 1px solid #C1CED9;
      padding: 8px 0;
      text-align: center;
    }
    .right-allign{
        text-align: right
    }
        </style>
      </head>
      <body>
        <header class="clearfix">
          <h1>INVOICE</h1>
          
          <div id="project">
            <div><span>CLIENT</span> ` + client.clientName + `</div>
            <div><span>ADDRESS</span> ` + client.clientLocation + `, ` + client.clientPostalCode + `</div>
            <div><span>EMAIL</span> <a href=` + client.clientEmail + `>` + client.clientEmail + `</a></div>
            <div><span>DATE</span>`+ date + `</div>
            <div><span>DUE DATE</span> `+dueDate+`</div>
          </div>
          <div id="company" class="clearfix">
            <div> ` + invoice.invoiceOwner + `</div>
            <div>` +  invoice.invoicerAddress + `</div>
            <div>` + invoice.invoicerPostcode + `,` + invoice.invoicerCity + `</div>
            <div>` + invoice.invoicerPhone + `</div>
            <div><a href="`+ invoice.invoicerEmail + `">` + invoice.invoicerEmail + `</a></div>
            <div>Bank N: ` + invoice.invoiceBankAccount + `</a></div>
            <div>Sort Code: ` + invoice.invoiceBankSort + `</a></div>
            <div>Invoice : #` + invoice.invoiceNumber + `</a></div>
          </div>
        </header>
        <main>
          <table>
            <thead>
              <tr>
                <th class="service">SERVICE</th>
                <th class="right-allign">PRICE</th>
                <th class="right-allign">QTY</th>
                <th class="right-allign">TOTAL</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="service">Programming</td>
                <td class="unit">£` + invoice.invoiceUnitPrice + `</td>
                <td class="qty">` + totalHours + `</td>
                <td class="total">£` + Math.round((invoice.invoiceUnitPrice * totalHours) * 100) / 100 + `</td>
              </tr>
              <tr>
                <td colspan="3" class="grand total">GRAND TOTAL</td>
                <td class="grand total">£`  + Math.round((invoice.invoiceUnitPrice * totalHours) * 100) / 100 + `</td>
              </tr>
            </tbody>
          </table>
          <div id="notices">
            <div>NOTICE:</div>
            <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 14 days.</div>
          </div>
        <footer>
          Invoice was created on a computer and is valid without the signature and seal.
        </footer>
      </body>
    </html>`};

