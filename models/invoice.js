var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InvoicesSchema = new Schema({
    invoiceNumber: Number,
    invoiceOwner: String,
    invoiceUnitPrice: Number,
    invoiceBankName: String,
    invoiceBankAccount: String,
    invoiceBankSort: String,
    invoicerPostcode: String,
    invoicerAddress: String,
    invoicerEmail: String,
    invoicerCity: String,
});

module.exports = mongoose.model('Invoices', InvoicesSchema);