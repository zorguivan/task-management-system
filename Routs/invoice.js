var express = require('express');
var invoiceRouter = express.Router();
var Invoices = require('../models/invoice');
var Client = require('../models/client');
var template = require('../template');
var fs = require('fs');
var pdf = require('html-pdf');
var mandrill = require('node-mandrill')('1r-M_OlgJNzpGQl2dFrnUg');
var pdfOptions = {
    format: "A4",
    orientation: "portrait",
    border: {
        top: "1.25cm",
        right: "1.25cm",
        left: "1.25cm",
        bottom: "1.25cm"
    }
};

invoiceRouter.route('/invoice')
    .post(function (req, ress) {
        Client.find({ '_id': req.body.client }, function (err, clientData) {
            Invoices.find({}, function (err, invoiceData) {
                var invoice = invoiceData[0];
                var client = clientData[0];
                pdf.create(template(invoice, client, req.body.hours / 10), pdfOptions).toFile('Invoices/Invoice#' + invoice.invoiceNumber + '.pdf', function (err, res) {
                    if (err) return console.log(err);
                    Invoices.update({ _id: invoice._id }, {
                        $set: {
                            invoiceNumber: invoice.invoiceNumber + 1
                        }
                    }, function (err) {
                        if (err) {
                            ress.send(err);
                            console.log(err)
                        }
                        console.log('Saved')
                        ress.send(200);
                    });
                    var pdfAttach = fs.readFileSync('Invoices/Invoice#' + invoice.invoiceNumber + '.pdf');
                    mandrill('/messages/send', {
                        message: {
                            to: [{ email: 'oussama@codeaddicts.io', name: 'Oussama Zorgui' }],
                            from_email: 'alerts@codeaddicts.io',
                            subject: "Invoice - " + invoice.invoiceNumber,
                            attachments:
                                [{
                                    content: pdfAttach.toString('base64'),
                                    name: "Invoice",
                                    type: "application/pdf"
                                }],
                        }
                    }, function (error, response) {
                        if (error) console.log(JSON.stringify(error));
                        else console.log(response);
                    });

                });

            });

        });
    });

module.exports = invoiceRouter;