var express = require('express');
var clientsRouter = express.Router();
var Client = require('../models/client');


clientsRouter.route('/client')
    .post(function (req, res) {
        var client = new Client();
        console.log('Saving Client');

        console.log(req.body.clientName)

        client.clientName = req.body.clientName;
        client.clientEmail = req.body.clientEmail;
        client.clientCompany = req.body.clientCompany;
        client.clientLocation = req.body.clientLocation;
        client.clientPostalCode = req.body.clientPostalCode;
        client.clientAccount = req.body.clientAccount;
        client.clientSortCode = req.body.clientSortCode;
        client.save(function (err) {
            if (err)
                res.send(err);
        res.send(200);
    });
});

clientsRouter.route('/clients')
    .get(function (req, res) {
        Client.find({}, function (err, docs) {
            res.send(docs)
    });
});

clientsRouter.route('/client/:id')
    .delete(function (req, res) {
        console.log(req.params.id)
        Client.remove({ _id: req.params.id }, function (err, ress) {
            if (err) {
                console.log(err)
                res.send(err)
            }
        res.send(200);
    });
});

module.exports = clientsRouter;