var express = require('express');
var logsRouter = express.Router();
var Log = require('../models/log');


logsRouter.route('/log')
    .post(function (req, res) {
        var log = new Log();
        console.log('Saving Log')
        console.log(req.body.taskId)
        console.log(req.body.startTime)

        log.taskId = req.body.taskId;
        log.startTime = req.body.startTime;
        log.save(function (err) {
            if (err)
                res.send(err);
            res.send(200);
        });
    });

logsRouter.route('/logs/:id')
    .get(function (req, res) {
        Log.find({ taskId: req.params.id }, function (err, docs) {
            if (err)
                res.send(err);

            res.send(docs)
        });
    });

logsRouter.route('/log/:id')
    .put(function (req, res) {
        Log.update({ _id: req.params.id }, {
            $set: {
                taskId: req.body.taskId,
                startTime: req.body.startTime,
                endTime: req.body.endTime,
                totalTime: req.body.totalTime,
                summary: req.body.summary
            }
        }, function (err) {
            if (err) {
                res.send(err);
                console.log(err)
            }
            console.log('Saved')
            res.send(200);
        });
        console.log(req.body.taskId,
            req.body.startTime,
            req.body.endTime,
            req.body.totalTime,
            req.body.summary)
    });

module.exports = logsRouter;